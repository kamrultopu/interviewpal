'use strict';
const hapi = require('hapi');
const server = new hapi.Server();
const inert = require('inert');
const good = require('good');
const goodconsole = require('good-console')
server.connection({port:4000});

server.register(inert,(err)=>{
	if(err){
		throw err;
	}
	server.route(require('./routes/endpoints/staticroutes'));
})

server.start((err)=>{
	if(err){
		throw err;
	}
	console.log('server running at:' , server.info.uri);
});

/*
server.register({
	register: good,
	options:{
		reporters:[{
			reporter: goodconsole,
			events:{
				response: "*",
				log:"*"
			}
		}]
	}
},(err)=>{
	if(err){
		throw err;
	}
	server.start((err)=>{
		if(err){
			throw err;
		}
		console.log('server running at:' , server.info.uri);
	});
});
*/



