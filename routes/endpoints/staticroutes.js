'use strict';
const staticHandler = require('../handler/staticHandler');


let Routes = [
{
	method: 'GET',
    path: '/',
    handler: staticHandler.getIndex
}];

module.exports = Routes;